<?php
require_once dirname(dirname(__FILE__)).'/config/DBconfig.php';
require_once dirname(dirname(__FILE__)).'/lib/Path.php';

Path::loadConfig();
Path::loadLib();

//usar $this para accesar a las propiedades y  llamar a otros métodos de la clase actual donde te encuentras.


//Cuando accesamos a los métodos y propiedades de una clase, usamos el operador  (->) 
//llamado Operador Flecha o técnicamente Operador de Objetos.

$app = new Router(); //Creo el objeto  
$app->viewSection();  //accedo al metodo

/*
       $connU = DB::getInstance();
       $salt = hash('sha512','');
       $pass = hash('sha512','123456'.$salt);
      
       $result = $connU->query("SELECT * from users");
     //  $stmt->bindParam(1, $userId, PDO::PARAM_INT);
       $result->execute();
	   $row = $result->fetchAll();
	print_r($row);

*/
