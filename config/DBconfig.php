<?php
/*
 * DBconfig.php
 * 
 * Copyright 2014  <daniel@caldentech>
 * 
 * Clase que contiene la configuracion del sitio.
 * self::$var si es variable 
 * this-> metodo
 */

class DBconfig{
	public static $dbDriver= 'mysql';  // se declaran como public static, para no tener que iniciarlo con new
	public static $host = 'localhost';
	public static $user = 'root';
	public static $password= '0x0f.remote';
	public static $database= 'remoteexecution';
	
	public static function dsn(){
		return self::$dbDriver . ':host=' . self::$host . ';dbname=' . self::$database;
	}

}
