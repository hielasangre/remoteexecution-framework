<?php
/*
 * GlobalConfig.php
 * 
 * Copyright 2014  <daniel@caldentech>
 * 
 * 
 * 
 */

class GlobalConfig{
	public static $action = 'seccion';
	public static $idSection = 'id';
	public static $urlLogin = 'login';
	public static $urlLinks = 'links';
	public static $urlAbout = 'about';
	public static $titleDefault = 'Cruzados';
		
}

