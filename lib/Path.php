<?php
/*
 * Path.php
 * 
 * Copyright 2015 <hielasangre@fuckingmachine>
 * 
 * Clase para dejar normalizados los paths  de configuracion, librerias, modelo, etc..
 */

class Path{
	public static function loadConfig(){		
		require_once dirname(dirname(__FILE__)).'/config/DBconfig.php'; 
		require_once dirname(dirname(__FILE__)).'/config/GlobalConfig.php'; 
		}
	
	public static function loadLib(){
		require_once dirname(dirname(__FILE__)).'/lib/DB.php';
		require_once dirname(dirname(__FILE__)).'/lib/Security.php';
		require_once dirname(dirname(__FILE__)).'/lib/Router.php';
		require_once dirname(dirname(__FILE__)).'/lib/FactoryShowMe.php';
		}
}



