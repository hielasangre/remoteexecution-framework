 <?php
/**
* Class DB
* Copyright 2015 <hielasangre@fuckingmachine>
* Patron Singleton 
*/
class DB {
   
   private static $instance = NULL;

   /**
    *
	* El constructor se establece en modo privado 
     * Por lo que nadie puede crear una nueva instancia  
    *
    */
   private function __construct() {
       /*         * * quizas luego especifique el nombre ed la db "quizas" ** */
   }
   /**
    *
    * Retorna la instancia o crea una nueva conexion
    *
    * @return object (PDO)
    *
    *
    */
   public static function getInstance() {
       if (!self::$instance) {
           self::$instance = new PDO(DBConfig::dsn(), DBConfig::$user, DBConfig::$password);
           self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }
       return self::$instance;
   }

   /**
    *
    * Como con el  constructor, marco __clone como privaod
    * Por lo que nadie puede crear una nueva instancia
    *
    */
   private function __clone() {
   
   }
}
