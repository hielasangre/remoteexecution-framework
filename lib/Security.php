<?php
/*
 * Security.php
 * Copyright 2015 <hielasangre@fuckingmachine>
 * 
 */


class Security{
	private static function getSection(){
		$section = filter_input(INPUT_GET, GlobalConfig::$action, FILTER_SANITIZE_STRING); // PHP >= 5.2.0
		return $section;
	}
	
	private static function getID(){
		$id = filter_input(INPUT_GET, GlobalConfig::$idSection, FILTER_SANITIZE_STRING); // PHP >= 5.2.0
		return $id;
	}
	
	public static function getSanitizeSection(){
		$section = self::getSection();
		$sanitizeString = filter_var($section, FILTER_SANITIZE_STRING);
		return $sanitizeString;
	}

	public static function getSanitizeID(){
		$id = self::getID();
		$sanitizeID = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
		return $sanitizeID;
	}	
}
