<?php
/*
 * Router.php
 * 
 * Copyright 2015 <hielasangre@fuckingmachine>
 */

class Router{
	private $id = NULL;
	private $section = NULL;
	public function __construct(){
		//$this->section = (empty(Security::getSanitizeSection())) ? 'indexdefault' : Security::getSanitizeSection();	  // usar empty si la version de php es >= 5.5.0
		$this->section = (checkEmpty(Security::getSanitizeSection())) ? 'Index' : ucwords(Security::getSanitizeSection());	 // usar trim si la version de php es <= 5.5.0
		// Operador ternario que comprueba que lo que reciba por get sean los parametros que especifico  y que si esta vacio 
		// me dirija a la pagina por default
		//$this->id = Security::getSanitizeID();										
		}
	public function viewSection(){
		FactoryShowMe::view($this->section); // Empleo el patron factory para visualizar las distintas secciones del sitio.
		}
}

