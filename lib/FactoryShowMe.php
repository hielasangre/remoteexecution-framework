<?php
/*
 * Copyright 2015 <hielasangre@fuckingmachine>
*/
 class viewController{
  public function __construct(){
  }
  
   public function getController($section){
	echo "Bienvenido a: ".$section;
  }
}

function checkEmpty($var) {
    if (strlen($var) >= 1) {
        return False; // No esta vacia
    } else {
        return True; // Esta Vacia
    }
}

abstract class FactoryShowMe{
    public static function view($section){
		$pathController = dirname(dirname(__FILE__)).'/model/controller/'.$section.'Controller.php';
        try {
            if (is_readable($pathController)) {
                $model = new viewController;
                 return $model->getController($section);
            } else {
                throw new Exception("No existe la seccion ingresada");
            }
        } catch (Exception $e) {
            echo 'Excepcion capturada: ',  $e->getMessage(), "\n";
        }
    }
}
